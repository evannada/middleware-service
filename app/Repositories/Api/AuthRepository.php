<?php

namespace App\Repositories\Api;

use Illuminate\Support\Facades\Cache;

class AuthRepository {

    static function getToken()
    {   
        if (Cache::has('token')) {
            $token = Cache::get('token');
        } else {
            $client = new \GuzzleHttp\Client();
            $res = $client->post(env('NS_API_HOST').'/api/internal/login', [
                'json' => [
                    'username' => env('NS_API_USERNAME'),
                    'password' => env('NS_API_PASSWORD')
                ],
            ]);
            // $statusCode = $res->getStatusCode();
            $response = json_decode($res->getBody());
            // $response = $res->getBody();

            // set to 23 hours
            Cache::put('token', $response->jwt, 82800);  
            $token = Cache::get('token');
        }
        
        return $token;
    }
}