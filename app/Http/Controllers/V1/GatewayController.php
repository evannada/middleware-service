<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Api\GatewayRepository;

class GatewayController extends Controller
{
    protected $gateway;

    public function __construct(GatewayRepository $gateway)
    {
        $this->gateway = $gateway;
    }

    public function show($mac)
    {
        $result = $this->gateway->findByMac($mac);

        return response()->json($result, $result['status']);
    }
}