<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (!$token = app('auth')->setTTL(20160)->attempt($request->only('email', 'password'))) {
            return response()->json([
                'status' => 'error',
                'message' => 'These credentials do not match our records.'
            ], 401);
        }

        $query = User::query();
        $query = $query->whereId(app('auth')->user()->id);
        $user = $query->first();

        return response()->json([
            'status' => 'success',
            'token' => $token,
            'data' => $user
        ]);
    }

    public function me()
    {
        $user = app('auth')->user();

        return response()->json([
            'status' => 'success',
            'user' => $user
        ],200);
    }
}
