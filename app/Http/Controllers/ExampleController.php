<?php

namespace App\Http\Controllers;

use App\Repositories\Api\AuthRepository;

class ExampleController extends Controller
{
    public function test()
    {
        $token = AuthRepository::getToken();
        
        return response()->json([
            'token' => $token
        ], 200);
    }
}
