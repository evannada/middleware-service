<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'email' => 'demosuperadmin@wiraenergi.com',
            'password' => app('hash')->make('demosuperadmin'),
            'remember_token' => str_random(100)
        ]);
    }
}
